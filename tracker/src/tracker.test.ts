import { Tracker, TrackerConfig, TrackerData } from "./Tracker";

describe("Tracker", () => {
  let mockConfig: TrackerConfig;

  beforeEach(() => {
    mockConfig = {
      bufferSize: 3,
      sendInterval: 1000,
      getUrlFn: jest.fn(() => "https://example.com"),
      getPageTitleFn: jest.fn(() => "Example Page"),
      getTimeStampFn: jest.fn(() => 1625043227167),
      sendDataFn: jest.fn(() => Promise.resolve()),
      subscribeBeforeDestroyFn: jest.fn(),
    };
  });

  describe("constructor", () => {
    it("should set buffer and lastCallTime properties", () => {
      const tracker = new Tracker(mockConfig);

      expect(tracker["buffer"]).toEqual([]);
      expect(tracker["lastCallTime"]).toEqual(1625043227167);
    });

    it('should subscribe to the "beforeunload" event', () => {
      const tracker = new Tracker(mockConfig);

      expect(mockConfig.subscribeBeforeDestroyFn).toHaveBeenCalled();
    });
  });

  describe("track", () => {
    it("should add an event to the buffer", () => {
      const tracker = new Tracker(mockConfig);

      tracker.track("Click", "Button");

      const expectedData: TrackerData[] = [
        {
          event: "Click",
          tags: ["Button"],
          url: "https://example.com",
          title: "Example Page",
          ts: 1625043227,
        },
      ];

      expect(tracker["buffer"]).toEqual(expectedData);
    });

    it("should call sendData if the buffer size is reached", async () => {
      mockConfig.bufferSize = 2;
      const tracker = new Tracker(mockConfig);

      await tracker.track("Click", "Button 1");
      await tracker.track("Click", "Button 2");
      expect(mockConfig.sendDataFn).toHaveBeenCalledTimes(1);
    });

    it("should call sendData if the send interval is exceeded", async () => {
      mockConfig.sendInterval = 1;
      console.log(mockConfig);
      const tracker = new Tracker(mockConfig);

      tracker.track("Click", "Button 1");
      expect(mockConfig.sendDataFn).not.toHaveBeenCalled();

      jest.advanceTimersByTime(2);
      expect(mockConfig.sendDataFn).toHaveBeenCalledTimes(1);
    });
  });

  describe("sendData", () => {
    beforeEach(() => {
      jest.useFakeTimers();
    });

    afterEach(() => {
      jest.useRealTimers();
    });

    it("should do nothing if the buffer is empty", async () => {
      const tracker = new Tracker(mockConfig);

      await tracker["sendData"]();

      expect(mockConfig.sendDataFn).not.toHaveBeenCalled();
    });

    it("should send the data to the server", async () => {
      const tracker = new Tracker(mockConfig);
      tracker.track("Click", "Button");

      await tracker["sendData"]();

      expect(mockConfig.sendDataFn).toHaveBeenCalledWith([
        {
          event: "Click",
          tags: ["Button"],
          url: "https://example.com",
          title: "Example Page",
          ts: 1625043227,
        },
      ]);
    });

    it("should call the success callback if provided", async () => {
      const successCb = jest.fn();
      const tracker = new Tracker({
        ...mockConfig,
        successCb,
      });

      tracker.track("Click", "Button");
      await tracker["sendData"]();

      expect(successCb).toHaveBeenCalledWith([
        {
          event: "Click",
          tags: ["Button"],
          url: "https://example.com",
          title: "Example Page",
          ts: 1625043227,
        },
      ]);
    });

    it("should clear the buffer and update lastCallTime if the data is successfully sent", async () => {
      const tracker = new Tracker(mockConfig);
      tracker.track("Click", "Button");

      await tracker["sendData"]();

      expect(tracker["buffer"]).toEqual([]);
      expect(tracker["lastCallTime"]).toEqual(1625043227167);
    });

    it("should call the error callback if provided", async () => {
      const errorCb = jest.fn();
      const tracker = new Tracker({
        ...mockConfig,
        sendDataFn: () => Promise.reject("Error sending data"),
        errorCb,
      });

      tracker.track("Click", "Button");
      await tracker["sendData"]();

      expect(errorCb).toHaveBeenCalledWith("Error sending data");
    });
  });
});
