import { Tracker } from "./tracker";

function setupTracker() {
  const tracker = new Tracker({
    bufferSize: 3,

    sendInterval: 1000,

    getPageTitleFn: () => document.title,

    getUrlFn: () => window.location.href,

    getTimeStampFn: () => Date.now(),

    subscribeBeforeDestroyFn: (cb) => {
      window.addEventListener("beforeunload", cb);
    },

    sendDataFn: async (data) => {
      await fetch("http://localhost:4000/track", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      });
    },

    errorCb: (err) => {
      console.error(err);
    },
  });

  (window as any).tracker = tracker;

  console.log("[Tracker] tracker initialized");
}

setupTracker();
