export interface TrackerData {
  event: string;
  tags: string[];
  url: string;
  title: string;
  ts: number;
}

interface TrackerModule {
  track(event: string, ...tags: string[]): void;
}

/**
 * Configuration options for Tracker.
 *
 * @interface TrackerConfig
 * @property {number} bufferSize - The maximum number of events to buffer before sending them to the server.
 * @property {number} sendInterval - The maximum amount of time, in milliseconds, to wait before sending the buffered events to the server.
 * @property {() => string} getUrlFn - A function that returns the URL of the current page.
 * @property {() => string} getPageTitleFn - A function that returns the title of the current page.
 * @property {() => number} getTimeStampFn - A function that returns the current timestamp.
 * @property {(body: TrackerData[]) => Promise<any>} sendDataFn - A function that sends the buffered events to the server.
 * @property {(cb: () => any) => any} subscribeBeforeDestroyFn - A function that allows subscribing to the "beforeunload" event, so that the tracker can send the remaining events to the server before the page is closed.
 * @property {(err: any) => any} [errorCb] - An optional error callback that is called when there is an error sending the events to the server.
 * @property {(data: TrackerData[]) => any} [successCb] - An optional success callback that is called when the events are successfully sent to the server.
 */
export interface TrackerConfig {
  bufferSize: number;
  sendInterval: number;
  getUrlFn: () => string;
  getPageTitleFn: () => string;
  getTimeStampFn: () => number;
  sendDataFn: (body: TrackerData[]) => Promise<any>;
  subscribeBeforeDestroyFn: (cb: () => any) => any;
  errorCb?: (err: any) => any;
  successCb?: (data: TrackerData[]) => any;
}

/**
 * Module that tracks user events and sends them to a server.
 *
 * @class Tracker
 * @implements {TrackerModule}
 */
export class Tracker implements TrackerModule {
  private buffer: TrackerData[];
  private lastCallTime: number;

  /**
   * Tracker constructor.
   *
   * @param {TrackerConfig} config - Configuration options for the tracker.
   */
  constructor(private config: TrackerConfig) {
    this.buffer = [];
    this.lastCallTime = 0;

    this.config.subscribeBeforeDestroyFn(() => {
      this.sendData();
    });
  }

  /**
   * Adds an event to the buffer.
   *
   * @param {string} event - The name of the event.
   * @param {...string} tags - The tags associated with the event.
   */
  async track(event: string, ...tags: string[]) {
    const ts = Math.floor(this.config.getTimeStampFn() / 1000);
    const url = this.config.getUrlFn();
    const pageTitle = this.config.getPageTitleFn();

    const data: TrackerData = {
      event: event,
      tags: tags,
      url: url,
      title: pageTitle,
      ts: ts,
    };

    this.buffer.push(data);

    const hasReachedBufferSize = this.buffer.length >= this.config.bufferSize;
    const hasExceededInterval =
      this.config.getTimeStampFn() - this.lastCallTime >=
      this.config.sendInterval;

    if (hasReachedBufferSize || hasExceededInterval) {
      await this.sendData();
    }
  }

  private async sendData() {
    if (this.buffer.length === 0) {
      return;
    }

    try {
      await this.config.sendDataFn(this.buffer);

      if (this.config.successCb) {
        this.config.successCb(this.buffer);
      }

      this.buffer = [];
      this.lastCallTime = this.config.getTimeStampFn();
    } catch (err) {
      if (this.config.errorCb) {
        this.config.errorCb(err);
      }
    }
  }
}
