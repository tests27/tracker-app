super-tracker

## Installation

1. Create a `.env` file using the `.env.example` template.

2. In the `tracker` folder, run the following commands:

   ```sh
   yarn
   yarn build
   ```

3. In the root folder, run the following commands:

```sh
yarn
yarn build
yarn start
```

4. Open your web browser and go to localhost:4000/1.html. Make sure that the message "[Tracker] tracker initialized" appears in the console.
