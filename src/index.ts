import { app } from "./app";
import { DB } from "./db";
import dotenv from "dotenv";

dotenv.config();

const PORT = process.env.PORT || 5000;

const main = async () => {
  await DB.getInstance();
  console.log("Connected successfully to server");

  app.listen(PORT, () => console.log(`Listening http://localhost:${PORT}`));
};

main();
