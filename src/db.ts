import { MongoClient } from "mongodb";
import dotenv from "dotenv";
dotenv.config();

export class DB {
  private static instance: MongoClient;

  private constructor() { }

  static async getInstance(): Promise<MongoClient> {
    const uri = process.env.MONGO_URL as string;

    if (!DB.instance) {
      const client = await MongoClient.connect(uri);
      DB.instance = client;
    }

    return DB.instance;
  }
}
