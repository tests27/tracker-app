import request from "supertest";
import { MongoMemoryServer } from "mongodb-memory-server";
import { app } from "./app";

describe("GET /test", () => {
  it("should return 200 OK", async () => {
    await request(app).get("/test").expect(200);
  });
});

describe("POST /track", () => {
  let mongod: any;

  beforeAll(async () => {
    mongod = await MongoMemoryServer.create();
    const uri = mongod.getUri();
    process.env.MONGO_URL = uri;
  });

  afterAll(async () => {
    await mongod.stop();
  });

  it("should store events in MongoDB", (done) => {
    const events = [
      {
        event: "pageview",
        tags: [],
        url: "http://localhost:5000/1.html",
        title: "My website",
        ts: 1675209600,
      },
    ];

    request(app)
      .post("/track")
      .send(events)
      .expect(201)
      .end((err, res) => {
        if (err) return done(err);
        done();
      });

    // expect(response.status).toBe(201);
  });

  it("should return index.html", (done) => {
    request(app)
      .get("/1.html")
      .expect(200)
      .expect("Content-Type", /html/)
      .end((err, res) => {
        if (err) return done(err);
        done();
      });
  });
});
