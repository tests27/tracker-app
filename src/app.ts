import express, { Request, Response } from "express";
import path from "path";
import { DB } from "./db";
import cors from "cors";

export const app = express();

app.use(cors());
app.use(express.json());

// Test endpoint
app.get("/test", (req: Request, res: Response) => {
  return res.sendStatus(200);
});

app.get(["/1.html", "/2.html", "/3.html"], (req: Request, res: Response) => {
  res.sendFile(path.join(__dirname, "../public", "index.html"));
});

app.get("/tracker", (req: Request, res: Response) => {
  res.sendFile(path.join(__dirname, "../tracker/dist", "lib.js"));
});

// POST endpoint to track events
app.post("/track", async (req: Request, res: Response) => {
  try {
    const data = req.body;

    // Validate body
    if (!Array.isArray(data)) {
      return res.status(422).send("Request body must be an array.");
    }

    const invalidData = data.some(
      (item) =>
        typeof item.event !== "string" ||
        !Array.isArray(item.tags) ||
        typeof item.url !== "string" ||
        typeof item.title !== "string" ||
        typeof item.ts !== "number"
    );

    if (invalidData) {
      return res.status(422).send("Request body contains invalid data.");
    }

    res.status(200).send("Events stored in MongoDB");

    const client = await DB.getInstance();
    const db = client.db("tracker-app");
    const collection = db.collection("events");

    await collection.insertMany(data);
  } catch (err) {
    console.log(err);
    res.status(500).send("Error storing events in MongoDB");
  }
});
